import re

def arithmetic_arranger(problems, show_answers=False):
	first_line = ""
	second_line = ""
	lines_line = ""
	sumx = ""
	string = ""

	if len(problems) > 5:
		return 'Error: Too many problems.'
	for problem in problems:

		first_num = problem.split(' ')[0]
		second_num = problem.split(' ')[2]
		symbol = problem.split(' ')[1]

		if symbol != '+' and symbol != '-':
			return "Error: Operator must be '+' or '-'."
		if not first_num.isnumeric() or not second_num.isnumeric():
			return "Error: Numbers must only contain digits."
		if (len(first_num) > 4 or len(second_num) > 4):
			return 'Error: Numbers cannot be more than four digits.'
		sum = ''
		if (symbol == '+'):
			sum = str(int(first_num) + int(second_num))
		if (symbol == '-'):
			sum = str(int(first_num) - int(second_num))

		length = max(len(first_num), len(second_num)) + 2
		top = str(first_num).rjust(length)
		bottom = symbol + str(second_num).rjust(length - 1)
		line = ""
		res = str(sum).rjust(length)
		for s in range (length):
			line += "-"

		if problem != problems[-1]:
			first_line += top + '    '
			second_line += bottom + '    '
			lines_line += line + '    '
			sumx += res + '    '
		else:
			first_line += top + '\n'
			second_line += bottom + '\n'
			lines_line += line
			sumx += res

	if show_answers:
		string = first_line + second_line + lines_line + '\n' + sumx
	else:
		string = first_line + second_line + lines_line
	return string

	print(f'\n{arithmetic_arranger(["24 + 852", "3801 - 2", "45 + 43", "123 + 49"])}')